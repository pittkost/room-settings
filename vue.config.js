module.exports = {
  baseUrl: 'http://room-settings.pittkost.com/',
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')

    svgRule.uses.clear()

    svgRule
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
      
  }
}