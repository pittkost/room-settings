import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isAirConditionOn: false,
    roomTemperature: 20,
    roomLight: 'darkness'
  },
  getters: {
    isAirConditionOn: ({ isAirConditionOn }) => isAirConditionOn,
    roomTemperature: ({ roomTemperature }) => roomTemperature,
    roomLight: ({ roomLight }) => roomLight,
  },
  mutations: {
    turnOnAirCondition (state) {
      state.isAirConditionOn = true
    },
    turnOffAirCondition (state) {
      state.isAirConditionOn = false
    },
    setRoomTemperature (state, newTemperature) {
      state.roomTemperature = newTemperature
    },
    setRoomLight (state, newLight) {
      state.roomLight = newLight
    }
  }
})
